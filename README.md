# Principles of Blockchains

Welcome! This is the repository for ECE 598 PV: Principles of Blockchains, Spring 2020 at University of Illinois, Urbana-Champaign. [Main website of the course](https://courses.grainger.illinois.edu/ece598pv/sp2020/).

## Discussion
We use Piazza for discussion.

## Assignments

- [Assignment 1](Assignment1). Due date 12:30PM, Jan 28, 2020.
- [Assignment 2](Assignment2). Due date 12:30PM, Feb 6, 2020.

## Midterm Project

- [Team formation](https://forms.gle/e4UXyXrHUJfancqS6). Please form teams of 2 and fill in [this form](https://forms.gle/e4UXyXrHUJfancqS6). Illinois login is required to fill this form. Due date 12:30PM, Feb 4, 2020.
- [Part 1](MidtermProject1). Updated: ~~Due date 12:30PM, Feb 11, 2020.~~ Due date 12:30PM, Feb 13, 2020.
- [Part 2](MidtermProject2). Due date 12:30PM, Feb 20, 2020.
- [Part 3](MidtermProject3). Due date 12:30PM, Feb 27, 2020.
- [Part 4](MidtermProject4). Due date 12:30PM, Mar 5, 2020.
- [Part 5](MidtermProject5). No submission required.
- [Part 6](MidtermProject6). Due date 12:30PM, ~~Mar 26~~ Mar 27, 2020. **Update: the link of demo registration is added.** **Update: now due at 12:30PM Mar 27.**

## Final Project
- [Team formation](https://forms.gle/4xMyhubpTPCVmF3f8). Please form teams of 4. Combination of two teams from midterm project is recommended. Illinois login is required to fill this form. Due date 12:30PM, Mar 24, 2020.
- [List of projects](FinalProject)

## Policy
Submissions later than due date will get 0 points.
